# Anaconda Environment

Anaconda Python distribution for the class

There are two environment specifications in this repository, `conda_env_slim.yml` and `conda_env.yml`. The `_slim` spec is a slim environment that only installs a few things:

* scikit-learn
* scikit-image (for working with image data)
* ipython (for a vastly improved REPL experience)
* scipy
* numpy

The full `conda_env.yml` spec is a full development environment, with testing frameworks, code formatting, and style/syntax checkers.

## Installing the environment
To build a virtual environment using the specification files, use `conda env create --file conda_env.yml`.

## Using Anaconda
For help using Anaconda Python, here are some resources:

* [Brief guide I wrote to Python virtual environments](https://wiki.cwpitts.com/languages/python/virtual_environments/)
* [Conda cheat sheet](https://docs.conda.io/projects/conda/en/4.6.0/_downloads/52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf)
